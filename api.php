<?php
//引入核心类库
require_once('./inc/core.php');
require_once('./config.php');

//记录所有请求
$receive_json = json_encode($_REQUEST, JSON_UNESCAPED_UNICODE);
logRes($receive_json, "access"); //记录登录日志

$bt = new Bt($bt_api_url, $bt_api_token);
header('Content-type: application/json');
$act = $_REQUEST['act']; //接收方法名字
//获取站点列表
if ($act == 'get_weblist') {
	$arr = $bt->Websites($search = '', $page = '1', $limit = '100', $type = '-1', $order = 'id desc', $tojs = '');
	$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
	logRes("入参" . $receive_json . ",返参" . $json, "get_weblist"); //记录获取站点列表日志
	echo $json;
	die;
}
//获取某个站点的域名列表
$id = $_REQUEST['id']; //站点id
if ($act == 'get_webinfo') {
	$bt = new Bt($bt_api_url, $bt_api_token);
	$arr = $bt->WebDoaminList($id, $list = true);
	$json = json_encode($arr, JSON_UNESCAPED_UNICODE);
	logRes("入参" . $receive_json . ",返参" . $json, "get_webinfo"); //记录获取站点域名列表
	echo $json;
	die;
}

//给某个站点添加域名
$id = $_REQUEST['id'];
$webname = $_REQUEST['webname'];
$domain = $_REQUEST['domain'];
if ($act == 'add_domain') {
	//获取所有的网站
	$list_arr = $bt->Websites($search = '', $page = '1', $limit = '100', $type = '-1', $order = 'id desc', $tojs = '');
	foreach ($list_arr['data'] as $k => $v) {
		if ($v['name'] == trim($webname)) {
			$web_id = $v['id'];
		}
	}
	if ($web_id == '') {
		$rt_json = '{"status": false, "msg": "通过name没有找到对应的网站id！"} ';
		logRes("入参" . $receive_json . ",自定义返参" . $rt_json,  "add_domain"); //记录添加日志
		echo $rt_json;
		die;
	}
	$arr = $bt->WebAddDomain($web_id, $webname, $domain);
	$json = json_encode($arr, 1);
	logRes("入参" . $receive_json . ",返参" . $json,  "add_domain"); //记录添加日志
	echo $json;
	die;
}


//给某个站点删除域名
$id = $_REQUEST['id'];
$webname = $_REQUEST['webname'];
$domain = $_REQUEST['domain'];
$port = $_REQUEST['port'];
if ($act == 'del_domain') {
	//获取所有的网站
	$list_arr = $bt->Websites($search = '', $page = '1', $limit = '100', $type = '-1', $order = 'id desc', $tojs = '');
	foreach ($list_arr['data'] as $k => $v) {
		if ($v['name'] == trim($webname)) {
			$web_id = $v['id'];
		}
	}
	if ($web_id == '') {
		$rt_json = '{"status": false, "msg": "通过name没有找到对应的网站id！"} ';
		logRes("入参" . $receive_json . ",自定义返参" . $rt_json,  "add_domain"); //记录添加日志
		echo $rt_json;
		die;
	}
	$arr = $bt->WebDelDomain($web_id, $webname, $domain, $port);
	$json = json_encode($arr, 1);
	logRes("入参" . $receive_json . ",返参" . $json, "del_domain"); //记录删除日志
	echo $json;
	die;
}


//1.5获取服务器整体使用资源概括
if ($act == 'GetNetWork') {
	$arr = $bt->GetNetWork();
	$json = json_encode($arr, 1);
	logRes("入参" . $receive_json . ",返参" . $json,  "GetNetWork");
	echo $json;
	die;
}


$rt_json = '{"status": false, "msg": "没有找到对应的方法！"} ';
logRes("入参" . $receive_json . ",自定义返参" . $rt_json,  "add_domain"); //记录添加日志
echo $rt_json;
die;
