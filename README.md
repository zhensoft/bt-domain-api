# 宝塔域名管理api中转平台

#### 介绍

外部调用这个api，这个api平台在调用宝塔的域名管理api，相当于一个中转平台，调用宝塔api需要ip白名单，这种中转以后就不需要白名单校验了，同时接口也做了优化，比如去掉站点id的参数。




#### 使用说明
把根目录的配置文件config.example.php修改成config.php然后再修改bt_api_url和bt_api_token，第一个是自己的宝塔ip，在网页地址栏或者宝塔界面左上角那串数字，把实例http://127.0.0.1:8888改成http://（宝塔ip，要带上端口号）；
第二个是密钥，在宝塔界面左边菜单的面板设置下面的api接口，接口配置里可以找到api密钥，改成自己的。然后ip白名单增加当前服务器的ip也就是第一个参数（一行一个ip），

其他说明：
disk.php 是php调用shell脚本，利用php语言处理返回一个json格式字符串
api.php中的是借助宝塔的api实现，获取的内容信息更多。

定时清理服务器日志和runtime可以使用宝塔的计划任务和宝塔软件商店的webhook，里面计划任务里面的脚本，可以通过外部远程http触发执行shell脚本达到清理的目的

shell脚本示例如下:


```
#!/bin/sh
#删除超过指定天数的日志文件&删除指定文件夹下面的所有问题一般指runtime

find  /www/wwwroot/orderduoxuanyi.com/log/*/  -mtime +2  -name "*.txt" -exec rm -rf  {}  \;
rm -rf   /www/wwwroot/orderduoxuanyi.com/App/Runtime/*  ;

find     /www/wwwroot/www.huifengdh.cn/log/*/  -mtime +2  -name "*.txt" -exec rm -rf  {}  \;
rm -rf   /www/wwwroot/www.huifengdh.cn/App/Runtime/*  ;

find     /www/wwwroot/plat.dhv3.tihuobang.com/log/*/  -mtime +2  -name "*.txt" -exec rm -rf  {}  \;
rm -rf   /www/wwwroot/plat.dhv3.tihuobang.com/App/Runtime/* ;

find     /www/wwwroot/plat.v2.lipingu.com/public/log/*/  -mtime +2  -name "*.txt" -exec rm -rf  {}  \;
rm -rf   /www/wwwroot/plat.v2.lipingu.com/runtime/* ;

find     /www/wwwroot/plat.ghua.lizengtong.com/public/log/*/  -mtime +2  -name "*.txt" -exec rm -rf  {}  \;
rm -rf   /www/wwwroot/plat.ghua.lizengtong.com/runtime/* ;
```

shell脚本说明：

find：Linux查找命令，用户查找指定条件的文件

/opt/rh/log/：需要进行清理的目标目录

-mtime：+10 ：数字代表天数

-name "*.log.*"：目标文件的类型，带有log的所有文件

-exec：固定写法

rm -rf：强制删除包括目录在内的文件

{} \;：固定写法，一对大括号+空格+\+;

 
重点说说mtime参数：

-mtime n 按照文件的更改时间来找文件，n为整数。

n 表示文件更改时间距离为n天

-n 表示文件更改时间距离在n天以内

+n 表示文件更改时间距离在n天以前

例：
-mtime 0 表示文件修改时间距离当前为0天的文件，即距离当前时间不到1天（24小时）以内的文件。

-mtime 1 表示文件修改时间距离当前为1天的文件，即距离当前时间1天（24小时－48小时）的文件。

-mtime＋1 表示文件修改时间为大于1天的文件，即距离当前时间2天（48小时）之外的文件

-mtime -1 表示文件修改时间为小于1天的文件，即距离当前时间1天（24小时）之内的文件

