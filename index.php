<?php
$php_self = $_SERVER['PHP_SELF'];
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$fronturl = $protocol . $_SERVER['HTTP_HOST'] . substr($php_self, 0, strrpos($php_self, '/'));
//文档增加token校验
$token = @$_REQUEST['token'];
$token = addslashes($token);
if ($token != 'wl905507') {
    echo ('access deny ,please input token !');
    die;
}

?>


<html>

<head>
    <title>管理网站域名接口</title>
    <meta charset='utf-8' />
</head>

<body>
    <b>接口及说明</b>
    <hr />

    1.1 获取apache或者nginx的站点列表
    <br />
    说明：<br />
    <?php echo $fronturl ?>/api.php?act=get_weblist <br />
    <br />
    <br />


    1.2获取某个站点下面域名信息
    <br />
    说明：<br />
    <?php echo $fronturl ?>/api.php?act=get_webinfo&id=12 <br />
    id :站点的id
    <br />
    <br />


    1.3给某个站点添加一个域名(可以不传入id了，只传入webname 2023年1月6日更新)
    <br />
    说明：<br />
    <?php echo $fronturl ?>/api.php?act=add_domain&id=12&webname=0.0.0.0&domain=1234567890.wlphp.tk<br />
    id:站点的id，webname站点的名字，domain：网站域名例如666.wlphp.tk:88
    <br />返回信息：<br />
    {"status": true, "msg": "\u57df\u540d\u6dfb\u52a0\u6210\u529f!"}<br />
    {"status": false, "msg": "\u6307\u5b9a\u57df\u540d\u5df2\u7ed1\u5b9a\u8fc7!"}
    <br />
    <br />


    1.4删除某个站点的其中一个域名(可以不传入id了，只传入webname 2023年1月6日更新)
    <br />
    说明：<br />
    <?php echo $fronturl ?>/api.php?act=del_domain&id=12&webname=0.0.0.0&domain=1234567890.wlphp.tk&port=80<br />
    id:站点的id，webname站点的名字，domain：网站域名例如666.wlphp.tk ,port:80
    <br />返回信息：<br />
    {"status": true, "msg": "\u5220\u9664\u6210\u529f"}
    <br />
    <br />

    1.5获取服务器整体使用资源概括
    <br />
    说明：<br />
    <?php echo $fronturl ?>/api.php?act=GetNetWork<br />
    <br />返回信息：<br />
    根据实际情况
    <br />
    <br />



</body>

</html>