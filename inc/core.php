<?php

/**
 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
 * 注意：服务器需要开通fopen配置
 * @param $word 要写入日志里的文本内容 默认值：空值
 */
function logRes($word = '', $dir = "test", $include_referer = true)
{
	// 日志文件目录
	$dirnew = "./log/" . $dir . "/";

	// 创建目录（如果不存在）
	if (!is_dir($dirnew)) {
		mkdir($dirnew, 0777, true);
	}

	// 日志文件名
	$logname = $dirnew . date("Ymd") . ".txt";

	// 打开或创建日志文件并加锁
	$fp = fopen($logname, "a");
	flock($fp, LOCK_EX);

	// 获取来路信息
	$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	$parsed_referer = parse_url($referer);
	$domain = isset($parsed_referer['host']) ? $parsed_referer['host'] : '';

	// 获取客户端IP地址
	$remote_ip = $_SERVER['REMOTE_ADDR'];

	// 构建日志内容
	$log_content = "执行日期：" . strftime("%Y-%m-%d %H:%M:%S", time()) . "\r\n";
	if ($include_referer) {
		$log_content .= "来路域名：" . $domain . "\r\n";
	}
	$log_content .= "来路IP地址：" . $remote_ip . "\r\n" . $word . "\r\n\r\n\r\n";

	// 写入并解锁、关闭文件
	fwrite($fp, $log_content);
	flock($fp, LOCK_UN);
	fclose($fp);
}




class Bt
{

	private $BT_KEY = "";  	//接口密钥
	private $BT_PANEL = "";	   		//面板地址

	/**
	 * 初始化
	 * @param [type] $bt_panel 宝塔接口地址
	 * @param [type] $bt_key   宝塔Api密钥
	 */
	public function __construct($bt_panel = null, $bt_key = null)
	{
		if ($bt_panel) $this->BT_PANEL = $bt_panel;
		if ($bt_key) $this->BT_KEY = $bt_key;
	}


	/**
	 * 获取网站列表
	 * @param string $page   当前分页
	 * @param string $limit  取出的数据行数
	 * @param string $type   分类标识 -1: 分部分类 0: 默认分类
	 * @param string $order  排序规则 使用 id 降序：id desc 使用名称升序：name desc
	 * @param string $tojs   分页 JS 回调,若不传则构造 URI 分页连接
	 * @param string $search 搜索内容
	 */
	public function Websites($search = '', $page = '1', $limit = '15', $type = '-1', $order = 'id desc', $tojs = '')
	{
		$url = $this->BT_PANEL . "/data?action=getData&table=sites";

		$p_data = $this->GetKeyData();
		$p_data['p'] = $page;
		$p_data['limit'] = $limit;
		$p_data['type'] = $type;
		$p_data['order'] = $order;
		$p_data['tojs'] = $tojs;
		$p_data['search'] = $search;

		$result = $this->HttpPostCookie($url, $p_data);

		$data = json_decode($result, true);
		return $data;
	}
	/**
	 * 获取网站域名列表
	 * @param [type]  $id   网站ID
	 * @param boolean $list 固定传true
	 */
	public function WebDoaminList($id, $list = true)
	{
		$url = $this->BT_PANEL . "/data?action=getData&table=domain";
		$p_data = $this->GetKeyData();
		$p_data['search'] = $id;
		$p_data['list'] = $list;
		$result = $this->HttpPostCookie($url, $p_data);

		$data = json_decode($result, true);
		return $data;
	}

	/**
	 * 添加域名
	 * @param [type] $id      网站ID
	 * @param [type] $webname 网站名称
	 * @param [type] $domain  要添加的域名:端口 80 端品不必构造端口,多个域名用换行符隔开
	 */
	public function WebAddDomain($id, $webname, $domain)
	{
		$url = $this->BT_PANEL . "/site?action=AddDomain";
		$p_data = $this->GetKeyData();
		$p_data['id'] = $id;
		$p_data['webname'] = $webname;
		$p_data['domain'] = $domain;
		$result = $this->HttpPostCookie($url, $p_data);

		$data = json_decode($result, true);
		return $data;
	}

	/**
	 * 删除网站域名
	 * @param [type] $id      网站ID
	 * @param [type] $webname 网站名
	 * @param [type] $domain  网站域名
	 * @param [type] $port    网站域名端口
	 */
	public function WebDelDomain($id, $webname, $domain, $port)
	{
		$url = $this->BT_PANEL . "/site?action=DelDomain";


		$p_data = $this->GetKeyData();
		$p_data['id'] = $id;
		$p_data['webname'] = $webname;
		$p_data['domain'] = $domain;
		$p_data['port'] = $port;
		$result = $this->HttpPostCookie($url, $p_data);

		$data = json_decode($result, true);
		return $data;
	}


	//获取服务器概况
	public function GetNetWork()
	{
		$url = $this->BT_PANEL . "/system?action=GetNetWork";
		$p_data = $this->GetKeyData();
		$result = $this->HttpPostCookie($url, $p_data);
		$data = json_decode($result, true);
		return $data;
	}



	/**
	 * 构造带有签名的关联数组
	 */
	public function GetKeyData()
	{
		$now_time = time();
		$p_data = array(
			'request_token'	=>	md5($now_time . '' . md5($this->BT_KEY)),
			'request_time'	=>	$now_time
		);
		return $p_data;
	}

	/**
	 * 发起POST请求
	 * @param String $url 目标网填，带http://
	 * @param Array|String $data 欲提交的数据
	 * @return string
	 */
	private function HttpPostCookie($url, $data, $timeout = 60)
	{
		//定义cookie保存位置
		$cookie_file = './' . md5($this->BT_PANEL) . '.cookie';
		if (!file_exists($cookie_file)) {
			$fp = fopen($cookie_file, 'w+');
			fclose($fp);
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}
}
